﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace IsItOnline
{
    public partial class EditList : PhoneApplicationPage
    {
        public ObservableCollection<Helpers.FileManagement.TargetSite> FullSiteList;

        public EditList()
        {
            InitializeComponent();
            RefreshPage();
        }

        private void RefreshPage()
        {
            var currentSettings = Helpers.FileManagement.LoadAppSettings();
            FullSiteList = new ObservableCollection<Helpers.FileManagement.TargetSite>();
            currentSettings.CurrentSiteList.ForEach(x => FullSiteList.Add(x));
            EditSiteList.DataContext = FullSiteList;
        }

        private void DeleteIcon_Click(object sender, RoutedEventArgs e)
        {
            Button clickedOn = (Button)sender;
            string targetAddress = clickedOn.Tag.ToString();
            Helpers.FileManagement.RemoveWatchedSite(targetAddress);
            RefreshPage();
        }


    }
}