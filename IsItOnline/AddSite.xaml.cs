﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Xml.Linq;
using System.Xml.Serialization;
using IsItOnline.Helpers;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace IsItOnline
{
    public partial class AddSite : PhoneApplicationPage
    {
        public AddSite()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            string Name = NameTextBox.Text;
            string Address = AddressTextBox.Text.ToLower();
            if (Address.IndexOf("http://") == -1 || Address.IndexOf("https://") == -1)
            {
                Address = String.Format("http://{0}", Address);
            }

            if (!string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(Address))
            {
                FileManagement.AddWatchedSite(Name, Address);
                ErrorList.Text = "";

                // Once we have added the site, navigate back.
                NavigationService.GoBack();
            }
            else
            {
                ErrorList.Text = "Please enter both a Name and an Address";
            }
        }

    }
}