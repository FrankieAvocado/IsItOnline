﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using IsItOnline.Resources;
using System.IO.IsolatedStorage;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;

namespace IsItOnline
{
    public partial class MainPage : PhoneApplicationPage
    {
       
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            // We don't want people navigating "back" from the home page
            // that wouldn't even make sense.
            while (NavigationService != null && NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }

            // We need to fill in the data
            FillDataFromSettings();

            // then do whatever was going to happen normally
            base.OnNavigatedTo(e);
        }

        private void FillDataFromSettings()
        {
            SiteListPanel.Children.Clear();
            var savedSettings = Helpers.FileManagement.LoadAppSettings();

            foreach (var site in savedSettings.CurrentSiteList)
            {
                var newChild = NewSiteStatus(site.Name);
                SiteListPanel.Children.Add(newChild);

                var toUpdate = (System.Windows.Shapes.Rectangle)newChild.Children.FirstOrDefault(x => x.GetType() == typeof(System.Windows.Shapes.Rectangle));
                if (toUpdate != null)
                {
                    testSite(site.Address, toUpdate);
                }
            }
        }

        private Grid NewSiteStatus(string siteName)
        {
            Grid container = new Grid();

            container.Margin = new System.Windows.Thickness(30,0,12,0);
           
            ColumnDefinition firstColumn = new ColumnDefinition(){
                Width = new System.Windows.GridLength(4, GridUnitType.Star)
            };
            ColumnDefinition secondColumn = new ColumnDefinition()
            {
                Width = new System.Windows.GridLength(1, GridUnitType.Star)
            };

            container.ColumnDefinitions.Add(firstColumn);
            container.ColumnDefinitions.Add(secondColumn);

            var unknownColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 255));
            
            System.Windows.Shapes.Rectangle upStatus = new System.Windows.Shapes.Rectangle()
            {
                Width=16,
                Height=16,
                Fill = unknownColor
            };

            TextBlock serverName = new TextBlock()
            {
                Text = siteName
            };

            serverName.SetValue(Grid.ColumnProperty, 0);
            upStatus.SetValue(Grid.ColumnProperty, 1);

            container.Children.Add(serverName);
            container.Children.Add(upStatus);

            return container;
        }

        private void testSite(string uri, System.Windows.Shapes.Rectangle targetElement)
        {
            WebClient web = new WebClient();
            try
            {
                Uri targetSite = new Uri(uri);
                web.DownloadStringCompleted += new DownloadStringCompletedEventHandler(asyncLoadFeed);
                web.DownloadStringAsync(targetSite, targetElement);
            }
            catch // an error occurred with this uri.  show the "danger" color to the user
            {
                targetElement.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 120, 40));
            }            
        }

        private void asyncLoadFeed(object Sender, DownloadStringCompletedEventArgs e)
        {
            bool success = false;
            if (e.Error == null)
            {
                success = true;
            }

            var toUpdate = (System.Windows.Shapes.Rectangle)e.UserState;
            var upColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 255, 0));
            var downColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0));

            toUpdate.Fill = success ? upColor : downColor;
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/EditList.xaml", UriKind.Relative));
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddSite.xaml", UriKind.Relative));
        }

    }
}