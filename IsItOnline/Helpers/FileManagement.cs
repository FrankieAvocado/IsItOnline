﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace IsItOnline.Helpers
{
    public static class FileManagement
    {
        public static void AddWatchedSite(string name, string address)
        {
            var xmlSerializer = new XmlSerializer(typeof(XElement));
            XElement currentSites = new XElement("Root", new XElement("Sites"));

            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists("IsItOnline") == false)
                {
                    storage.CreateDirectory("IsItOnline");
                }

                if (storage.FileExists("IsItOnline\\SiteList.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("IsItOnline\\SiteList.xml", System.IO.FileMode.Open, storage))
                    {
                        currentSites = XElement.Load(isoStream);
                    }
                }
            }

            XElement newSite = new XElement("Site",
                                            new XElement("Name", name),
                                            new XElement("Address", address),
                                            new XElement("LastUp", DateTime.MinValue.ToString()),
                                            new XElement("IsUp", false.ToString()));

            currentSites.Element("Sites").Add(newSite);

            saveSiteListToFile(currentSites);
        }

        public static void RemoveWatchedSite(string address)
        {
            var xmlSerializer = new XmlSerializer(typeof(XElement));
            XElement currentSites = new XElement("Root", new XElement("Sites"));

            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.DirectoryExists("IsItOnline") == false)
                {
                    storage.CreateDirectory("IsItOnline");
                }

                if (storage.FileExists("IsItOnline\\SiteList.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("IsItOnline\\SiteList.xml", System.IO.FileMode.Open, storage))
                    {
                        currentSites = XElement.Load(isoStream);
                    }
                }
            }

            List<XElement> toRemove = new List<XElement>();

            var sitesElement = currentSites.Descendants("Sites").FirstOrDefault();

            if (sitesElement != null)
            {
                toRemove.AddRange(sitesElement.Descendants("Site").Where(x => x.Element("Address").Value == address));
            }

            foreach (var removeMe in toRemove)
            {
                removeMe.Remove();
            }

            saveSiteListToFile(currentSites);
        }

        public static SavedAppSettings LoadAppSettings()
        {
            SavedAppSettings returnMe = new SavedAppSettings();
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists("IsItOnline\\SiteList.xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("IsItOnline\\SiteList.xml", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, storage))
                    {
                        returnMe.CurrentSiteList.AddRange(loadSiteList(XElement.Load(isoStream)));
                    }
                }
            }
            return returnMe;
        }

        private static List<TargetSite> loadSiteList(XElement XMLData)
        {
            var siteList = from site in XMLData.Descendants("Site")
                           select new TargetSite
                           {
                               Name = site.Element("Name").Value,
                               Address = site.Element("Address").Value,
                               LastUp = DateTime.Parse(site.Element("LastUp").Value),
                               IsUp = Boolean.Parse(site.Element("IsUp").Value)
                           };

            return siteList.ToList();
        }

        private static void saveSiteListToFile(XElement currentSites)
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (!storage.DirectoryExists("IsItOnline"))
                {
                    storage.CreateDirectory("IsItOnline");
                }


                // If our sitelist file doesn't exist yet, then we can open the isostream as a "create"
                if (!storage.FileExists("IsItOnline\\SiteList.Xml"))
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("IsItOnline\\SiteList.xml", System.IO.FileMode.Create, System.IO.FileAccess.Write, storage))
                    {
                        currentSites.Save(isoStream);
                    }
                }
                else
                {
                    // IMPORTANT:  If the sitelist already does exist, then truncate the file before saving the new list, 
                    // or you will run into trouble if the list is shorter than the existing file.  
                    // There will be leftover characters at the end from the previous data and it will break
                    // ALL of the xml validation :-(
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("IsItOnline\\SiteList.xml", System.IO.FileMode.Truncate, System.IO.FileAccess.Write, storage))
                    {
                        currentSites.Save(isoStream);
                    }
                }

            }
        }

        public class SavedAppSettings
        {
            public List<TargetSite> CurrentSiteList { get; set; }

            public SavedAppSettings()
            {
                CurrentSiteList = new List<TargetSite>();
            }
        }

        public class TargetSite
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public DateTime LastUp { get; set; }
            public bool IsUp { get; set; }
        }
    }
}
